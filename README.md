# Single Responsibility Prinsipi

Hər bir class sistemin yalnız bir funksionallığına və ya tək hissəsinə cavabdeh olmalıdır. Qısaca olaraq, class-ın yalnız 1 dəyişmə səbəbi  olmalıdır.
Funksionallıqları class-lara ayırmaq bizə bir çox üstünlük verir:

* Class-ın təkrar istifadəsi
* Problemin qısa müddətdə tapılması və həlli
* və s.


## Açıqlama

`az.codestream.wrong` package-ində yerləşən UserGroup faylında bir çox funksionallıq var ki,
əslində bu class daxilində olmamalıdır. Məsələn mail göndərmək, çapa vermək üçün funksionallıq
ayrı-ayrı classlarda olmalıdır. Qeyd edim ki, bu metodlar reallıqdan uzaqdır və sadəcə prinsipin nümayişi üçündür.

`az.codestream.correct` package-ində isə funksionallıqlar class-lar arasında parçalanıb. Burada interface-lərdən,
Design Pattern-lərdən də istifadə etmək olardı ( Məsələn: Factory, AbstractFactory və s.)
Ancaq kodu sadə saxlamaq üçün birbaşa class-lardan istifadə edilib.

`az.codestream.common` package-ində isə ümumi class olan User class-ı göstərilib.
