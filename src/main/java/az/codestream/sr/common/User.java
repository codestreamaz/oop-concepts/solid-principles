package az.codestream.sr.common;

import lombok.Data;

@Data
public class User {
    private String name;
}
