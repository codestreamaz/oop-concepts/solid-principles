package az.codestream.sr.wrong;

import az.codestream.sr.common.User;

import java.util.List;

public class UserGroup {
    private List<User> users;

    public void add(User user) {
        // Add new user to the group
    }

    public void remove(User user) {
        // Remove user from the group
    }

    public void email(User user) {
        // Send email to the user
    }

    public void makeReport(User user) {
        // Generate report about the user
    }

    public void printReport(User user) {
        makeReport(user);
        // Make report, then print it
    }
}
